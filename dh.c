/*********************************************************************
* Filename:   dh.c
* Author:     weijiel6
*********************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <limits.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>



int change_dec(char a);
int compute(int g, int m, int p);







int compute(int g, int m, int p) {
	int r;
	int result = 1;

	while (m > 0) {
		r = m % 2;
	
	
		if (r == 1){
			result = (result*g) % p;
		}
		g = g*g % p;
	
		m = m / 2;
	}
	return result;
}


int change_dec(char a) {
	int result;
	if(isalpha(a)) {
		result = a - 87;
	} else {
		result = a - 48;
	}
	
	return result;
}

int main( int argc, char **argv ) {
	int g = 15;
	int p = 97;
	char command[50];
	strcpy(command, "openssl sha256 dh.c >v.txt" );
	system(command);
	FILE *fp;
	fp = fopen("v.txt", "r");
	while((fgetc(fp) != EOF) && (fgetc(fp) != ' ')) {
		
	}
	char first_arg = fgetc(fp);
	char second_arg = fgetc(fp);
	
	int result_a;
	int result_b;
	
	result_a = change_dec(first_arg);
	result_b = change_dec(second_arg);
	
	int b = (result_a * 16) + result_b;
	int B = compute(g, b, p);
	printf("%d %d\n", b, B);
	
	char send_B[3];
	sprintf(send_B, "%d\n", B); 
	printf("The string for the num is %s\n", send_B);
	
	
	int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent * server;

    char buffer[256];

    portno = 7800;


    /* Translate host name into peer's IP address ;
     * This is name translation service by the operating system*/
     
    server = gethostbyname("172.26.37.44");
    /* Building data structures for socket */

    bzero((char *)&serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;

    bcopy(server->h_addr_list[0], (char *)&serv_addr.sin_addr.s_addr, server->h_length);

    serv_addr.sin_port = htons(portno);

    /* Create TCP socket -- active open
    * Preliminary steps: Setup: creation of active open socket*/
    

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
    {
        perror("ERROR opening socket");
        exit(0);
    }

    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("ERROR connecting");
        exit(0);
    }
	
	bzero(buffer, 256);

    strcpy(buffer, "weijiel6\n");

    n = write(sockfd, buffer, strlen(buffer));
	
	bzero(buffer, 256);
	
	strcpy(buffer, send_B);
	
	n = write(sockfd, buffer, strlen(buffer));
	
	
    if (n < 0)
    {
        perror("ERROR writing to socket");
        exit(0);
    }

    bzero(buffer, 256);

    n = read(sockfd, buffer, 255);

    if (n < 0)
    {
        perror("ERROR reading from socket");
        exit(0);
    }
	
	int A = atoi(buffer);
	
	int keyB = compute(A, b, p);
	char send_keyB[10];
	sprintf(send_keyB, "%d\n", keyB); 
	printf("The string for the send_keyB is %s\n", send_keyB);
	
	
	bzero(buffer, 256);
	
	strcpy(buffer, send_keyB);
	
	n = write(sockfd, buffer, strlen(buffer));
	
	
	bzero(buffer, 256);

    n = read(sockfd, buffer, 255);
	
    printf("%s\n", buffer);
	//printf("%d\n", A);
	
   	return 0;
}

