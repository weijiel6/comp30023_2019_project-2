/*********************************************************************
* Filename:   crack.c
* Author:     weijiel6
*********************************************************************/



#include "sha256.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/stat.h>
int check_guess(BYTE secrets[], BYTE hash[], int size);
int get_password(char *line, FILE *fp);




int
get_password(char *line, FILE *fp) {
	int i = 0, c;
	while(((c = fgetc(fp)) != EOF) && (c !='\n') && (c != '\r')) {
		line[i++] = c;
	}
	line[i] = '\0';
	return 1;
}




int check_guess(BYTE secrets[], BYTE hash[], int size) {
	//for(int i=0; i<32;i++){
	//	printf("%02x",hash[i]);
	//}
	//printf("\n");
	int compare_size = 32;
	int flag;
	
	int turn = (size /32) + 1;
	//printf("size is %d, turn is %d\n",size, turn);
	//flag = true;
	for(int i = 1 ; i < turn ; i++) {
		flag = -1;
		for(int k = (i - 1) *compare_size; k < i *compare_size; k++) {
			if (secrets[k] != hash[k-((i - 1) *compare_size)]) {
				flag = 0;
				break;
			}else{
				flag=1;
			}
			
		}
		if(flag == 1){
			//int num;
			//num = flag * i;
			//printf("this is true for %d\n", i);
			//printf("true!!!!\n");
			//printf("return value should be %d\n",num);
			return flag * i;
		}
	}
	//printf("%d\n", flag);
	return flag;
}


int main( int argc, char **argv )
{
	
	/* check we have 0 argument, test for guessing password*/
	if( argc < 2 )
	{	
		
		FILE *fp;
		FILE *fp1;
		int pwd4_size;
		fp1 = fopen("out.txt", "w");
		BYTE secrets[320];
		fp = fopen("pwd4sha256", "rb");
		fread(secrets, 320, 1, fp);
		/*Move file point at the end of file.*/
    	fseek(fp, 0, SEEK_END);
     
    	/*Get the current position of the file pointer.*/
    	pwd4_size=ftell(fp);
		
		fseek(fp, 0, SEEK_SET); 
		printf("size is  %d\n", pwd4_size);
		for(int i = 0; i < 320; i++){
			
			printf("%02x", secrets[i]);
			if(((i+1) %32) == 0) {
				printf("\n");
			}
		}
		//printf("all done\n");
		fclose(fp);
		char guess[5];
		int secret_num;
		SHA256_CTX ctx;
		BYTE data[SHA256_BLOCK_SIZE];
		for(int i = 32; i < 127; i++) {
			for(int j = 32; j < 127; j++) {
				for(int l = 32; l < 127; l++) {
					for(int k = 32; k < 127; k++) {
						sprintf(guess, "%c%c%c%c", i, j, l, k);
						//printf("%s\n",guess);	
										
						sha256_init(&ctx);
						sha256_update(&ctx, (BYTE*)guess, strlen(guess));
						sha256_final(&ctx, data);
						if (check_guess(secrets, data, pwd4_size) > 0) {
							secret_num = check_guess(secrets, data, pwd4_size);
							printf("%s %d\n",guess, secret_num);
							fprintf(fp1, "%s %d\n", guess, secret_num);							
						}
						
						//printf("%s\n", guess);
					}
				}
			}
		}
		
		guess[5] = '\0';
		
		FILE *fp3;
		BYTE long_secrets[640];
		fp3 = fopen("pwd6sha256", "rb");
		fread(long_secrets, 640, 1, fp3);
		int pwd6_size;
		/*Move file point at the end of file.*/
    	fseek(fp3, 0, SEEK_END);
     
    	/*Get the current position of the file pointer.*/
    	pwd6_size=ftell(fp3);
    	
    	fseek(fp3, 0, SEEK_SET);
    	printf("size is  %d\n", pwd6_size);
		for(int i = 0; i < 640; i++){
			
			printf("%02x", long_secrets[i]);
			if(((i+1) %32) == 0) {
				printf("\n");
			}
		}
		printf("\n");
		
		char long_guess[7];
		int long_secret_num;
		SHA256_CTX long_ctx;
		BYTE long_data[SHA256_BLOCK_SIZE];
		for(int i = 48; i < 58; i++) {
			for(int j = 48; j < 58; j++) {
				for(int l = 48; l < 58; l++) {
					for(int k = 48; k < 58; k++) {
						for(int n = 48; n < 58; n++) {
							for(int m = 48; m < 58; m++) {
								sprintf(long_guess, "%c%c%c%c%c%c", i, j, l, k, n, m);
								//printf("%s\n",guess);	
												
								sha256_init(&long_ctx);
								sha256_update(&long_ctx, (BYTE*)long_guess, strlen(long_guess));
								sha256_final(&long_ctx, long_data);
								if (check_guess(long_secrets, long_data, pwd6_size) > 0) {
									long_secret_num = check_guess(long_secrets, long_data, pwd6_size);
									long_secret_num = long_secret_num + 10;
									//printf("long secret num should be %d\n", long_secret_num);
									printf("%s %d\n",long_guess, long_secret_num);
									fprintf(fp1, "%s %d\n", long_guess, long_secret_num);							
								}
							}
						}
					}	
				}
			}
		}
		long_guess[7] = '\0';
		
		char alpha_guess[7];
		int alpha_secret_num;
		SHA256_CTX alpha_ctx;
		BYTE alpha_data[SHA256_BLOCK_SIZE];
		for(int i = 97; i < 124; i++) {
			for(int j = 97; j < 124; j++) {
				for(int l = 97; l < 124; l++) {
					for(int k = 97; k < 124; k++) {
						for(int n = 97; n < 124; n++) {
							for(int m = 97; m < 124; m++) {
								sprintf(alpha_guess, "%c%c%c%c%c%c", i, j, l, k, n, m);
								//printf("%s\n",guess);													
								sha256_init(&alpha_ctx);
								sha256_update(&alpha_ctx, (BYTE*)alpha_guess, strlen(alpha_guess));
								sha256_final(&alpha_ctx, alpha_data);
								if (check_guess(long_secrets, alpha_data, pwd6_size) > 0) {
									alpha_secret_num = check_guess(long_secrets, alpha_data, pwd6_size);
									alpha_secret_num = alpha_secret_num + 10;
									//printf("long secret num should be %d\n", long_secret_num);
									printf("%s %d\n",alpha_guess, alpha_secret_num);
									fprintf(fp1, "%s %d\n", alpha_guess, alpha_secret_num);							
								}
							}
						}
					}	
				}
			}
		}
		alpha_guess[7] = '\0';
		
		return 0;
	}

	if( argc == 2 )
	{
		
		return 0;
	}
	
	if( argc == 3 )
	{	
		FILE *fp1;
		fp1 = fopen(argv[2], "rb");
		int fp1_size;
		fseek(fp1, 0, SEEK_END);
    	fp1_size=ftell(fp1);
    	fseek(fp1, 0, SEEK_SET);
		BYTE secrets[fp1_size];
		fread(secrets, fp1_size, 1, fp1);
		for(int i = 0; i < fp1_size; i++){
			
			printf("%02x", secrets[i]);
			if(((i+1) %32) == 0) {
				printf("\n");
			}
		}
		FILE *fp;
		char line[10000];
		fp = fopen(argv[1], "r");
		int secret_num;
		for(int i = 0; i < 10000 ; i++) {
			get_password(line, fp);
			SHA256_CTX ctx;
			BYTE data[SHA256_BLOCK_SIZE];
			sha256_init(&ctx);
			sha256_update(&ctx, (BYTE*)line, strlen(line));
			sha256_final(&ctx, data);
			if (check_guess(secrets, data, fp1_size) > 0) {
				secret_num = check_guess(secrets, data, fp1_size);
				printf("%s %d\n", line, secret_num);							
			}
		}
		fclose(fp);
		fclose(fp1);
		return 0;
	}
	
	

}

